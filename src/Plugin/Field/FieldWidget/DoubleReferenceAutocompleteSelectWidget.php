<?php

namespace Drupal\double_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides an autocomplete/select widget for the double_reference field type.
 *
 * The primary reference uses autocomplete while the added reference uses
 * a select field.
 *
 * @FieldWidget(
 *   id = "double_reference_autocomplete_select",
 *   label = @Translation("Double reference autocomplete/select"),
 *   description = @Translation("Autocomplete for primary and select for added reference."),
 *   field_types = {
 *     "double_reference"
 *   }
 * )
 */
class DoubleReferenceAutocompleteSelectWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = parent::formElement($items, $delta, $element, $form, $form_state);

    // Get the reference target entity type from the storage settings.
    $ar_target_type = $this->getFieldSetting('ar_target_type');

    // Get the field settings.
    $settings = $this->fieldDefinition->getSettings();

    // Set the label on the primary reference field, if one is in settings.
    if (!empty($settings['pr_label'])) {
      $widget['target_id']['#title'] = $settings['pr_label'];
      $widget['target_id']['#title_display'] = 'before';
    }

    // Get the settings for the added reference field.
    $ar_bundles = $settings['added_reference']['ar_bundles'];
    $ar_label = !empty($settings['added_reference']['ar_label']) ? $settings['added_reference']['ar_label'] : '';
    $ar_weight = !empty($settings['added_reference']['ar_weight']) ? $settings['added_reference']['ar_weight'] : -50;
    $ar_required = !empty($settings['added_reference']['ar_required']) ? $settings['added_reference']['ar_required'] : FALSE;

    // Get the existing value, if any, for the added reference field.
    $default = isset($items[$delta]) ? $items[$delta]->ar_target_id : NULL;
    if (!empty($default)) {
      $default = \Drupal::entityTypeManager()->getStorage($ar_target_type)->load($default);
    }

    // Get the options.
    $options = $this->getTaxonomyOptions((array) $ar_bundles);

    // Build the added reference form field.
    $widget['ar_target_id'] = [
      '#type' => 'select',
      '#default_value' => $default,
      '#options' => $options,
      '#weight' => $widget['target_id']['#weight'] + $ar_weight,
      '#required' => $this->isDefaultValueWidget($form_state) ? FALSE : $ar_required,
    ];

    // Set the label on the added reference field, if one is in settings.
    if (!empty($ar_label)) {
      $widget['ar_target_id']['#title'] = $ar_label;
      $widget['ar_target_id']['#title_display'] = 'before';
    }

    $widget['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $widget;
  }

  /**
   * Get an options list from taxonomy vocabularies.
   *
   * @param array $vocabularies
   *   The list of vocabularies to use.
   *
   * @return array
   *   An options list of terms.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTaxonomyOptions(array $vocabularies) {
    // Start off with empty so the field can be left empty.
    $options = [
      NULL => '',
    ];

    /** @var \Drupal\taxonomy\TermStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');

    foreach ($vocabularies as $vid => $vocab) {
      // Empty check because checkboxes leave the vid in there but set to 0.
      if (!empty($vocabularies[$vid])) {
        // Load all the terms for this vocabulary and format them into
        // an options list.
        $terms = $storage->loadTree($vid);
        foreach ($terms as $term) {
          $options[$term->tid] = FieldFilteredMarkup::create($term->name);
        }
      }
    }

    return $options;
  }

  /**
   * Validates the element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (empty($element['target_id']['#value']) && !empty($element['ar_target_id']['#value'])) {
      $form_state->setError($element['target_id'], new TranslatableMarkup('%field is required when %ar_field has a value.', [
        '%field' => $element['target_id']['#title'],
        '%ar_field' => $element['ar_target_id']['#title'],
      ]));
    }
  }

}
