<?php

namespace Drupal\double_reference\Plugin\EntityUsage\Track;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\entity_usage\Plugin\EntityUsage\Track\EntityReference;

/**
 * Tracks usage of entities related in entity_reference fields.
 *
 * @EntityUsageTrack(
 *   id = "double_reference",
 *   label = @Translation("Double Reference"),
 *   description = @Translation("Tracks relationships created with 'Double Reference' fields."),
 *   field_types = {"double_reference"},
 * )
 */
class DoubleReference extends EntityReference {

  /**
   * {@inheritdoc}
   */
  public function getTargetEntities(FieldItemInterface $item) {
    $target_entities = parent::getTargetEntities($item);

    $item_value = $item->getValue();
    if (empty($item_value['ar_target_id'])) {
      return $target_entities;
    }

    $ar_target_type = $item->getFieldDefinition()->getSetting('ar_target_type');
    if (!$this->entityTypeManager->getStorage($ar_target_type)->load($item_value['ar_target_id'])) {
      return $target_entities;
    }

    $target_entities[] = $ar_target_type . '|' . $item_value['ar_target_id'];

    return $target_entities;
  }

}
