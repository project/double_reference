CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
Double Reference provides a single field item that contains two entity
reference fields.

One field is the primary that uses all the normal entity reference field forms,
the other is an attached field which has most, but not all, of the same options.
The two fields work together to form one field.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/double_reference

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/double_reference


REQUIREMENTS
------------

Core Entity Reference.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

Add the field in the normal method. It is listed as "Double Reference" in
the "Reference" section. It works similar to an Entity Reference field but
has additional configuration for the added reference field.


MAINTAINERS
-----------

Current maintainer: Michelle Cox ( https://www.drupal.org/u/michelle )
